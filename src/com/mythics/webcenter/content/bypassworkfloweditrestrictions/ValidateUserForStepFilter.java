package com.mythics.webcenter.content.bypassworkfloweditrestrictions;

import static com.mythics.webcenter.content.bypassworkfloweditrestrictions.StringConstants.IS_BYPASS_VALIDATE_WORKFLOW_USER;
import static com.mythics.webcenter.content.bypassworkfloweditrestrictions.StringConstants.VALIDATE_USER_FOR_STEP_FILTER_ENABLED;
import static com.mythics.webcenter.content.bypassworkfloweditrestrictions.Utils.trace;
import static com.mythics.webcenter.content.bypassworkfloweditrestrictions.Utils.traceVerbose;
import static intradoc.shared.SharedObjects.getEnvValueAsBoolean;
import intradoc.common.ExecutionContext;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.Workspace;
import intradoc.server.Service;
import intradoc.shared.FilterImplementor;

import com.mythics.webcenter.content.bypassworkfloweditrestrictions.StringConstants.ServiceName;

/**
 * 
 * @author Jonathan Hult
 * 
 */
public class ValidateUserForStepFilter implements FilterImplementor {

	public int doFilter(final Workspace ws, final DataBinder binder, final ExecutionContext ctx) throws DataException, ServiceException {
		traceVerbose("Enter doFilter for validateUserForStep for BypassWorkflowEditRestrictions");

		try {
			if (getEnvValueAsBoolean(VALIDATE_USER_FOR_STEP_FILTER_ENABLED, false)) {
				if (ctx instanceof Service) {
					final String serviceName = ((Service) ctx).getServiceData().m_name;
					trace("Service name: " + serviceName);

					// Only bypass workflow check for checkout, checkin_sel
					// (existing content items) or metadata updates
					if (serviceName != null && (serviceName.equals(ServiceName.CHECKOUT_SUB.name()) || serviceName.equals(ServiceName.CHECKIN_SEL_FORM_SUB.name()) || serviceName.equals(ServiceName.CHECKIN_SEL_SUB.name()) || serviceName.equals(ServiceName.WORKFLOW_CHECKIN_SUB.name()) || serviceName.equals(ServiceName.GET_UPDATE_FORM.name()) || serviceName.equals(ServiceName.UPDATE_DOCINFO_SUB.name()))) {
						((Service) ctx).setConditionVar(IS_BYPASS_VALIDATE_WORKFLOW_USER, true);
						trace("Set serviceConditionVar " + IS_BYPASS_VALIDATE_WORKFLOW_USER + " to true");
					}
				}
			}
		} finally {
			traceVerbose("Exit doFilter for validateUserForStep for BypassWorkflowEditRestrictions");
		}
		return CONTINUE;
	}
}
