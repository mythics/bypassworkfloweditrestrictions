package com.mythics.webcenter.content.bypassworkfloweditrestrictions;

import static com.mythics.webcenter.content.bypassworkfloweditrestrictions.StringConstants.IS_BYPASS_VALIDATE_WORKFLOW_USER;
import static com.mythics.webcenter.content.bypassworkfloweditrestrictions.StringConstants.VALIDATE_USER_FOR_STEP;
import static com.mythics.webcenter.content.bypassworkfloweditrestrictions.Utils.trace;
import static intradoc.shared.PluginFilters.filter;

import intradoc.common.ServiceException;
import intradoc.data.DataException;

/**
 *
 * @author Jonathan Hult
 *
 */
public class WorkflowDocImplementor extends intradoc.server.workflow.WorkflowDocImplementor {
	@Override
	public void validateUserForStep(final boolean isValidateType, final String type, final String errorMessageSuffix, final boolean isAllowAdmin) throws DataException, ServiceException {
		trace("Enter validateUserForStep");

		try {
			// Call validateUserForStep filter
			filter(VALIDATE_USER_FOR_STEP, this.m_workspace, this.m_binder, this.m_service);

			// Determine if workflow check should be bypassed
			// This is set in the checkWorkflow filter
			final boolean isBypassCheckWorkflow = m_service.isConditionVarTrue(IS_BYPASS_VALIDATE_WORKFLOW_USER);
			if (isBypassCheckWorkflow) {
				trace("Skipping workflow check");
			} else {
				// Do workflow check
				trace("Doing standard workflow check");
				super.validateUserForStep(isValidateType, type, errorMessageSuffix, isAllowAdmin);
			}
		} finally {
			trace("Exit validateUserForStep");
		}
	}
}
