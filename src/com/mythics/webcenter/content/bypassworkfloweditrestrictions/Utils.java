package com.mythics.webcenter.content.bypassworkfloweditrestrictions;

import static com.mythics.webcenter.content.bypassworkfloweditrestrictions.StringConstants.TRACE_SECTION;

import intradoc.common.Report;

/**
 *
 * @author Jonathan Hult
 *
 */
public class Utils {

	/**
	 * This method verbose traces output to the WebCenter Content console.
	 *
	 * @param message
	 *            The String to trace to the WebCenter Content console.
	 */
	protected static void trace(final String message) {
		Report.trace(TRACE_SECTION, message, null);
	}

	/**
	 * This method verbose traces output to the WebCenter Content console.
	 *
	 * @param message
	 *            The String to trace to the WebCenter Content console.
	 */
	protected static void traceVerbose(final String message) {
		if (Report.m_verbose) {
			trace(message);
		}
	}
}
