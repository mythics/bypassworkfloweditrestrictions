package com.mythics.webcenter.content.bypassworkfloweditrestrictions;

public class StringConstants {
	/**
	 *
	 * @author Jonathan Hult
	 *
	 */

	protected enum ServiceName {
		CHECKOUT_SUB, CHECKIN_SEL_FORM_SUB, CHECKIN_SEL_SUB, WORKFLOW_CHECKIN_SUB, GET_UPDATE_FORM, UPDATE_DOCINFO_SUB, WORKFLOW
	}

	protected static final String VALIDATE_USER_FOR_STEP = "validateUserForStep";
	protected static final String IS_BYPASS_VALIDATE_WORKFLOW_USER = "isBypassValidateWorkflowUser";
	protected static final String VALIDATE_USER_FOR_STEP_FILTER_ENABLED = "BypassWorkflowEditRestrictions_ValidateUserForStepFilterEnabled";
	protected static final String TRACE_SECTION = "bypassworkfloweditrestrictions";
}
