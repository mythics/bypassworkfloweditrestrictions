﻿# BypassWorkflowEditRestrictions

## Component Information
* Created by: Mythics
* Author: Jonathan Hult
* Last Updated: build_3_20160126
* License: MIT

## Overview
This WebCenter Content component disables the check during metadata updates, checkouts and checkins so that a user is always able to edit a content item even while it is in workflow.

**This component can be very dangerous as it bypasses the typical approver security requirement of a workflow. It should be used with extreme caution.**

* Class Aliases:
	- WorkflowDocImplementor: Core - Override validateUserForStep method to add validateUserForStep filter. If the service condition variable, isBypassValidateWorkflowUser, is set to true during validateUserForStep filter, the normal validateUserForStep will not execute.

* Filters:
	- validateUserForStep: Called during WorkflowDocImplementor.validateUserForStep.

* Filter Implementors:
	- ValidateUserForStepFilter: Called during validateUserForStep filter. This first checks if BypassWorkflowEditRestrictions_ValidateUserForStepFilterEnabled is enabled. If so, it checks the service is one of the following: CHECKOUT_SUB, CHECKIN_SEL_FORM_SUB, CHECKIN_SEL_SUB, WORKFLOW_CHECKIN_SUB, GET_UPDATE_FORM, UPDATE_DOCINFO_SUB. If so, the service condition variable, isBypassValidateWorkflowUser, is set to true.
	
* Preference Prompts:
	- BypassWorkflowEditRestrictions_ValidateUserForStepFilterEnabled: boolean determining whether "validateUserForStep" filter code is executed.

## Compatibility
This component was built upon and tested on the version listed below, this is the only version it is known to work on, but it may work on older/newer versions.

* 10.1.3.5.1 (111229) (Build:7.2.4.105)
* 11.1.1.8.1DEV-2014-01-06 04:18:30Z-r114490 (Build:7.3.5.185)

## Changelog
* build_3_20160126
	- Added code to fix security bug on not calling super.validateUserForStep
* build_2_20140303
	- Added WORKFLOW_CHECKIN_SUB as a service
	- Change from overriding method checkWorkflow to validateUserForStep
	- Change from checkWorkflow filter to validateUserForStep filter
* build_1_20140226
	- Initial component release